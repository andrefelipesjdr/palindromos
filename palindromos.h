#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define _GNU_SOURCE
/*
Autor: Andre Felipe Jose Santos
andrefelipesjdr@gmail.com
04/12/19
gcc version 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.11)
*/

/* faz a leitura do arquivo de entrada */
char * lerArquivo();

/* inverte uma dada string */
void inverte(char*);

/* recebe uma string e vai add caracteres ate se tornar palindromo.*/
void adicionaCaracter(char*);

/* varifica se uma dada string eh um palindromo caso sim imprime, caso nao chama adicionaCaracter */
void manipula(char*);

/* chamada das funcoes */
void inicia();
