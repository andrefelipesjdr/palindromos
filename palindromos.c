#include "palindromos.h"
/*
Autor: Andre Felipe Jose Santos
andrefelipesjdr@gmail.com
04/12/19
gcc version 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.11)
*/

char *lerArquivo(){
  FILE *f = fopen("input.txt", "r");
  size_t len = 50;
  char *linha = (char*)malloc(len);
  char *palavras = (char*)malloc(128);

  if(!f){
    perror("input.txt");
    exit(1);
  }
  while (getline(&linha, &len, f) > 0) {
    strcat(palavras,linha);
  }
  if(linha){
    free(linha);
  }
  fclose(f);
  return palavras;
}

void inverte(char *s){
    int length = strlen(s) ;
    int c, i, j;
    for (i = 0,j = length - 1; i < j; i++,j--){
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

void adicionaCaracter(char* palavra) {
  int tam = strlen(palavra);
  int it = 0;
  char aux;
  char *palavraInv = (char*) malloc(tam);
  for (int i = tam; i >= 0; i--) { // pula posicoes invalidas
    if(i == tam){
      i-=2;
    }
    it++; // numero de iteracoes
    if(palavra[i-1] == palavra[i+1] && it == 1){ // verificao de letras repetidas gerando um bloco equivalente
      i-=2; // caso for um bloco o salto eh feito
      aux = palavra[i];
    } else {
      aux = palavra[i];
    }
    palavra[tam] = aux; // apos achar a caracter a ser repetido ele eh add
    tam++;
    palavra[tam] = '\0';
    strcpy(palavraInv, palavra);
    inverte(palavraInv);
    if (strcmp(palavraInv,palavra) == 0) { // verificao se ja eh um palindromo
      printf("%d\n", it);
      break;// caso for, sair do loop
    }
  }
  // exit(1);
}

void manipula(char* palavras){
  int tam = strlen(palavras);
  char *palavra = (char*)malloc(tam);
  char *palavraInv = (char*)malloc(tam);
  int i = 0;
  int j = 0;
  // itera ate o fim da string de palavras
  // separa palavra por palavra, substituindo por 1 caracter por caracter ao passar.
  while (palavras[i] != '\0') {
    if(palavras[i] != '\n' && palavras[i] != '1'){ // entra caso tenha chegado no fim da palavra ou em um caracter ja copiado
      palavra[j] = palavras[i];
      palavras[i] =  '1'; // substituicao
      i++;j++;
    } else { // palavra separada
      palavra[j] = '\0'; // tratando a string levando em conta seu novo tamanho
      strcpy(palavraInv,palavra);
      inverte(palavraInv);
      if(strcmp(palavra,palavraInv) == 0){ // caso o seu inverso for igual imprime 0
        if(palavra[0] != '\0'){// verificao se a palavra eh valida
          printf("0\n");
        }
      } else { // add caracter
        adicionaCaracter(palavra);
      }
      i++;
      j = 0;
      // printf("abaixo %s aqui %c\n", palavras, palavras[i]);
    }
  }
}

void inicia() {
  char *palavras = lerArquivo();
  manipula(palavras);
}
