# Problema palindromos

> A resolucao proposta, foi implementada para a seleção do Summer Job Accenture 2020, a qual propoe encontrar qual é o número mínimo de caracteres que precisam ser adicionados ao fim da palavra para que ela se torne um palíndromo.

Explicacao do problema [Maratona] (https://maratona.algartelecom.com.br/anteriores/3mineira/prova.pdf)

**Compilador:** *gcc version 5.4.0 20160609*
**SO:** *(Ubuntu 5.4.0-6ubuntu1~16.04.11)*

**Arquivo de entrada para a execucao:** input.txt
**Compilacao:** make
**Execucao:** ./p

