CFLAGS=-lm -Wall

SAIDA=saidaTP*


CC:=gcc
EXEC:=p
RM:=rm -rf

HRD:=$(wildcard *.h)
SRC:=$(wildcard *.c)
OBJ:=$(SRC:.c=.o)


#--------------------/------------------------


all:$(EXEC)

$(EXEC): $(HDR) $(OBJ)
	$(CC) $^ -o $(EXEC) $(CFLAGS)

%.o: %.c
	$(CC) $^ -c $(CFLAGS)

limpar:
	clear

dbug:
	$(CC) $(SRC) -o $(EXEC) $(CFLAGS) -g

clean:
	$(RM) $(OBJ) $(EXEC) $(SAIDA)

